<article>
	<section>
	  <header class="row bg-light">
		<h2 class="row ml-0 text-muted h3">Bonjour <?=isset($member_name)?$member_name:"";?>, bienvenue sur votre profil</h2>
		<?php 
		if(1>2):?>
			<p>Vous êtes sur la nouvelle page <?= __FILE__ ?>.
		<?php
		endif;
		if(!isset($member_name)):?>
		<p class="text-success">Message pour le webmaster : $member_name est inconnu. Controller non trouvé?<br>
		<small><i>Fichier <?= __FILE__ ?></small></i></p>
		<?php 
		endif;?>
	  </header>
		<fieldset class = "fieldset_profil">
		  <?=isset($level)?"<p>Vous êtes connecté. Votre niveau: $level</p>":""?>
		  <?php
		  if(isset($member) && $member->level < 3){
			?>
			<ul>
				<li>Utilisateurs et visiteurs
					<ul>
					  <li><a href="index.php?page=statistiques">Statistiques</a></li>
					  <li><a href="index.php?page=users">Utilisateurs</a></li>
					</ul>
				</li>
				<li>Pages
					<ul>
					  <li><a href="<?= $website->page_url;?>page-creation">Créer une nouvelle page</a></li>
					  <li><a href="<?= $website->page_url;?>pages-index&categorie=pages">Modifier les pages existantes</a></li>
					</ul>
				</li>
			  <?php
		  }
		  if(isset($member) && $member->level < 2){
			?>
				<li>Config
					<ul>
					  <li><a href="index.php?page=config-file-read">Modifier la configuration du site</a></li>
					</ul>
				</li>
				<li>Bac à sable
					<ul>
					  <li><a href="<?= $website->page_url;?>bac">Travaux de développement en cours</a></li>
					</ul>
				</li>
				<li>Edition des clients réservée à l'administrateur du site
					<ul>
					  <li><a href="<?= $website->page_url;?>client-index">Clients</a></li>
					</ul>
				</li>
				<li>Edition des utilisateurs réservée à l'administrateur du site
					<ul>
					  <li><a href="<?= $website->page_url;?>user-index">Utilisateurs</a></li>
					</ul>
				</li>
				<li>Membres
				<ul>
				  <li><a href="<?= $website->page_url;?>member-register">Ajouter un membre</a></li>
				  <li><a href="<?= $website->page_url;?>member-index">Modifier les membres</a></li>
				</ul>
			  </li>
			  <li>Base de données
				  <ul>
					<li><a href="index.php?page=backup-manager">Backup manager</a></li>
					<li><a href="https://phpmyadmin.cluster021.hosting.ovh.net/index.php?db=marcharnssmarc">OVH myAdmin (marcharnssmarc.mysql.db)</a></li>
				   </ul>
			  </li>
			  <li>Code: comment écrire un message dans un contrôleur lors de travaux ?
				  <ul>
					<li><a href="index.php?page=ecrire_un_message">Ecrire un message</a></li>
				   </ul>
			  </li>
			</ul>
			<?php
		  }
		  ?>
		</fieldset>
	  </section>
</article>