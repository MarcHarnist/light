﻿<?php
/**             Contrôleur __client-update
*                   Marc L. Harnist
*                       28/05/2020
**/ 

  $message = new Message;
  $update = new Database;
  $dbTable = "TABLE_USER";
	if(defined('TABLE_USER'))
	{
		$dbTable = TABLE_USER;
		$users = $update->read_table($dbTable);
	}
	else echo '<p style="margin:50px;"><q>Erreur dans la base de donnée dans le fichier ' . __FILE__ . '<br>
					' . $dbTable . ' est inconnu. Mettre à jour le fichier de configuration du site.</q></p>';

   $resultat = $update->update_table_users($dbTable, $_POST);
  
  if($resultat === true)
  {
    /**   AFFICHAGE DES NEWS SUR LES TRAVAUX EN COURS      */
    
    echo $website->message("Travaux en cours", "Gestion de la redirection en cas de modification réussie.", "lightgreen");

  	header ('Location: ' . $website->redirection('user-index'));

	  $message->setGreen("Modification réussie.");
  }	
  
