<?php
/** Controller user.php
*   Marc L. Harnist
*   2020-08-06
*
*   Rights are check in class Page.php / checkUserRightsForAdmin()
*/
	$count=1;
	$ancre=0;
	$database = new Database;
	$dbTable = "TABLE_USER";
	if(defined('TABLE_USER'))
	{
		$dbTable = TABLE_USER;
		$users = $database->read_table($dbTable);
	}
	else echo '<p style="margin:50px;"><q>Erreur dans la base de donnée dans le fichier ' . __FILE__ . '<br>
					' . $dbTable . ' est inconnu. Mettre à jour le fichier de configuration du site.</q></p>';
