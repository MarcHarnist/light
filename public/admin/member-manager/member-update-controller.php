﻿<?php
/**             Contrôleur member-update
*                   Marc L. Harnist
*                       28/08/2018
*
*   Autorisation limitée au webmaster
*/  $website->membersPermissions(1, $member);

  $update = new Database;
  $db_table = TABLE_MEMBER;//Base de données
  $ancre = $update->update_table_members($db_table, $_POST);
  $page_url_redirection = $website->page_url . 'member-index#' . $ancre . ''; 
  //var_dump($page_url_redirection); die();
  header ('Location: ' . $page_url_redirection);