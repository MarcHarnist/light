## ============ SERVER ============

light: ## Launch Light CMS in browser
	php -S 127.0.0.1:8000 -t ./
.PHONY: light

serv: ## Launch PHP and Apache
	cd C:\Users\marc\Desktop\projet && powershell -Command "Start-Process 'php' '-S localhost:80' -NoNewWindow"
	php -S localhost:8080
.PHONY: serv

stop: ## Stop PHP and Apache
	npx kill-port 8080
	npx kill-port 80
.PHONY: stop

hello: ## Say "Hello" in terminal
	@echo "Hello it's Marc"
.PHONY: hello